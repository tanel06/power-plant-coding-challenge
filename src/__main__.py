from flask import Flask, request, render_template
from flask_restful import Resource, Api
import json
import logging
import pandas as pd
import matplotlib.pyplot as plt

app = Flask("Power")
api = Api(app)

#Manage logging on a file
logging.basicConfig(filename='log/app.log', level=logging.DEBUG)

@app.route("/")
def index():
    """
    Some tests : 

    data = formatData('Payload/powerplants3.json')
    power = computePower(293, 13.4, 50.8, 50, data)
    power1 = computePower(480, 13.4, 50.8, 60, data)
    power2 = computePower(480, 13.4, 50.8, 0, data)
    power3 = computePower(910, 13.4, 50.8, 60, data)
    carpo = getPowerPlantByName('windpark2', data)
    print(power3)
    """

    return "Welcome to my coding challenge !!!"

@app.route('/plotpowerplants')
def plot_plantpowerplantsDistribution():
    """
        Plot the distribution of powerplants

    """

    #plant distribution
    fig = plt.figure()
    data = pd.read_json('result/result_powerplants.json')
    data = data[data['p'] != 0] 
    data = data.rename(columns={'p': 'Power'})
    #print(data)
    data['Power'].plot.pie(labels=data['name'], autopct="%1.1f%%", figsize=(6,6))
    #plt.show()
    fig.savefig('static/images/plotpowerplant.png')
    return render_template('powerplantDist.html', name = 'Power Plants Distribution', url ='static/images/plotpowerplant.png')

def formatData(file):
    """
        format the json file to dict

        Args:
          file (file) : the json file to format

        return:
          d (dict): the new format 
    """
    d = []
    f = open(file,) 
    data = json.load(f)
    
    for p in data['powerplants']:
        name = p.pop('name')
        d.append({name: p})
    
    f.close()

    return d

def getPowerPlantByName(name, powerplants):
    """
        get the caracteristics of a powerplant given a name

        Args:
          name (str) : powerplant name
          powerplants (dict) : list of powerplant
        
        return:
          d (dict) : powerplant characteristic
    """
    result = ''
    for powerplant in powerplants:
        cle = list(powerplant.keys())[0]
        if cle == name:
            result = powerplant[cle]
            break
    return result

def computePower(load, gaz, kero, wind, powerplants):
    """
        compute the power of each powerplans

        Args:
          load (int)   : The load is the amount of energy (MWh) that need to be generated during one hour.
          gaz  (float) : the price of gas per MWh
          kero (float) : the price of kerosine per MWh.
          wind (int)   : percentage of wind
          powerplants (dict) : list of powerplant

        Returns:
          powers (list): list of power plants and their power production


    """
    cost = dict()
    powers = []

    # compute cost for each powerplant
    for powerplant in powerplants:
        cle = list(powerplant.keys())[0]
        value = list(powerplant.values())[0]
        if value['type'] == "windturbine":
            cost[cle] = 0
        elif value['type'] == "gasfired":
            cost[cle] = gaz/value['efficiency']
        else:
            cost[cle] = kero/value['efficiency']

    pLoad = load

    # compute the merit-order.
    merit_order = sorted(list(cost.items()), key=lambda x : x[1])


    #Compute power for each powerplant
    for e in merit_order:
        powerplant_name = e[0]
        powerplant_meta = getPowerPlantByName(powerplant_name, powerplants)

        pmax = powerplant_meta['pmax']
        pmin = powerplant_meta['pmin']
        p = 0
        
        if powerplant_meta['type'] == 'windturbine':
            pWind = pmax*(wind/100)

            #to ensure that there is no waste of production
            if pLoad > pWind :
                p = pWind
                pLoad = pLoad - p
            else:
                p = pLoad
                pLoad = 0

        elif powerplant_meta['type'] == 'turbojet':
            if pLoad > pmax : 
                p = pmax
                pLoad = pLoad - p
            else:
                p = pLoad
                pLoad = 0
        else:
            if pLoad > pmax :
                p = pmax - pmin
                pLoad = pLoad - p
            else:
                p = pLoad
                pLoad = pLoad - p
            
            

        powers.append({'name' : powerplant_name, 'p': round(p,1)})

    return powers

class Production(Resource):
    """
    post method
    post args via URL
    Ex : http://localhost:8888/productionplan?load=293&gaz=13.4&kerozine=50.8&wind=50
    """
    def post(self):
        
        load = request.args.get('load', type=int)
        gaz  = request.args.get('gaz', type=float)
        kero = request.args.get('kerozine', type=float)
        wind  = request.args.get('wind', type=int)

        # format our json file
        payload_sample = 'Payload/powerplants3.json'
        data = formatData(payload_sample)

        # compute power
        powerList = computePower(load, gaz, kero, wind, data)

        # save the result in result directory
        with open('result/result_powerplants.json', 'w') as f:
            json.dump(powerList, f,  indent=4)
        
        return powerList, 200  # return data with 200 OK


# '/usersproductionplan' is our entry point 
api.add_resource(Production,"/productionplan")           

app.run(host="0.0.0.0",port=8888)

