# Powerplant Coding Challenge


## Description of the Challenge

* [Clic here](https://github.com/gem-spaas/powerplant-coding-challenge)


## Requirements

1. Python 3+
2. Flask
3. Flask-RESTful
4. pip
5. Matplotlib
6. Pandas



## Getting started

### On Windows OS

In a terminal, go to the root of the project and execute the following commands : 
1. ```.\install.bat```  (to install the dependencies)
2. ```.\start.bat```  (to launch the project)


### On Linux OS

1. Install the project dependencies by placing yourself at the same location as the "requirements.txt" file and type in your terminal: ``` pip install -r .\requirements.txt ```.
2. Launch the project by typing : ``` python3 src``` or ``` ./start.sh ```

## Lauch the REST API

1. Use postman to get the productionplan of each powerplant
3. Then, choose the POST method
3. The URL to be used has the following form : 
``` http://localhost:8888/productionplan?load=293&gaz=13.4&kerozine=50.8&wind=50```

where :
* **load :** the amount of energy (MWh) that need to be generated during one hour.
* **gaz :**  the price of gas per MWh
* **kerozine :** the price of kerosine per MWh.
* **wind :** percentage of wind

4. The API response is stored in the ```result``` directory as a json file.

**__Note__** : To test another payload sample, you need to change the value of the ```payload_sample``` variable in the post method of Production class. The available samples are stored in the payload directory.


## To launch the docker image of the project
In a terminal, go to the root of the project and execute the following commands :

1. ```docker-compose up```
2. On the address bar of your browser, type: ```http://localhost:8888```

## To visualise the power distribution obtained with our solver

On the address bar of your browser, type: ```http://localhost:8888/plotpowerplants```

![Power distribution](./static/images/plotpowerplant.png)
